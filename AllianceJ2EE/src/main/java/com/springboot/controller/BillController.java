package com.springboot.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.springboot.service.BillService;

@Controller
@RequestMapping("/bill")
public class BillController {
	
	@Autowired
	private BillService billService;
	

	@RequestMapping("/{namessss}/edit")
	public String hiBill3(@PathVariable String namessss) {
		System.out.println(" THIS IS PATH NAME - " + namessss);
		return "bill/index";
	}
	
	@RequestMapping("/hi")
	public String hiBill() {
		return "bill/hi";
	}
	
	@RequestMapping("/hi2")
	public String hiBill2(HttpServletRequest request, ModelMap map) {
		String nameField = request.getParameter("nameField");
		String passField = request.getParameter("passField");
		boolean userExists = billService.validateIfUserExists(nameField, passField);
		
		if(!userExists) {
			map.addAttribute("message", "USER " + nameField + " DOES NOT EXIST");
		} else {
			map.addAttribute("message", "USER " + nameField + " DOES EXIST");
		}
		
		System.out.println("NAME FIELD " + nameField);
		System.out.println("PASS FIELD " + passField);
		map.addAttribute("userNameField", nameField);
		
		
		return "bill/2ndjsp";
	}
}