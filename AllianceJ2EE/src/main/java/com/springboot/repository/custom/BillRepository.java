package com.springboot.repository.custom;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class BillRepository {
	
	@PersistenceContext
	private EntityManager em;
	
	public boolean userExists(String username, String password) {
		boolean exists = false;
		
		Session session = em.unwrap(Session.class);
		StringBuilder stringQuery = new StringBuilder("SELECT * FROM user WHERE username = :username AND password = :password");
		SQLQuery query = session.createSQLQuery(stringQuery.toString());
		query.setParameter("username", username);
		query.setParameter("password", password);
		List list = query.list();
//		exists = (list != null && list.size() > 0);
		
		if(list != null && list.size() > 0) {
			exists = true;
		}
	
		return exists;
	}
}
