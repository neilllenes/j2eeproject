package com.springboot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.repository.custom.BillRepository;

@Service
public class BillService {

	@Autowired
	private BillRepository billRepository;
	
	public boolean validateIfUserExists(String username, String password) {
//		boolean exists = false;
//		if(username != null && username.length() > 5) {
//			exists = true;
//		}
		boolean exists = billRepository.userExists(username, password);
		return exists;
	}
}
